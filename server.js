const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.PORT || 5000;

// Conexión a la base de datos MongoDB
mongoose.connect('mongodb://localhost:27017/crudmernstack', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Conexión exitosa a MongoDB'))
  .catch(err => console.error('Error en la conexión a MongoDB:', err));

app.use(bodyParser.json());
app.use(cors());

// Definir el modelo de datos (ejemplo: para una entidad "Producto")
const productoSchema = new mongoose.Schema({
  nombre: { type: String, required: true },
  precio: { type: Number, required: true },
});
const Producto = mongoose.model('Producto', productoSchema);

// Rutas CRUD para los productos
app.get('/api/productos', async (req, res) => {
  try {
    const productos = await Producto.find();
    res.json(productos);
  } catch (err) {
    res.status(500).json({ error: 'Error al obtener los productos' });
  }
});

app.post('/api/productos', async (req, res) => {
  try {
    const { nombre, precio } = req.body;
    const producto = new Producto({ nombre, precio });
    await producto.save();
    res.json(producto);
  } catch (err) {
    res.status(500).json({ error: 'Error al crear el producto' });
  }
});

app.put('/api/productos/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { nombre, precio } = req.body;
    const producto = await Producto.findByIdAndUpdate(id, { nombre, precio }, { new: true });
    res.json(producto);
  } catch (err) {
    res.status(500).json({ error: 'Error al actualizar el producto' });
  }
});

app.delete('/api/productos/:id', async (req, res) => {
  try {
    const { id } = req.params;
    await Producto.findByIdAndDelete(id);
    res.json({ message: 'Producto eliminado correctamente' });
  } catch (err) {
    res.status(500).json({ error: 'Error al eliminar el producto' });
  }
});

app.listen(PORT, () => {
  console.log(`Servidor backend escuchando en http://localhost:${PORT}`);
});
